<?php
include("Header.php"); 
// include("database/connect.php"); 
?>
<style>
.Setting{ 
  background-color: #CE0600;
}
</style>
<!------------------------------------------------------Header------------------------------------------------------>

<div class="row"> 
  <div class="col-md-12 col-sm-12"> 
    <div class="alert" style="background-color: #CE0600; color: #fff;"  role="alert">
      <i class="fa fa-fw fa-cog" aria-hidden="true"></i>
      Add Setting
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12"> 
    <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-fw fa-hourglass"></i>&nbsp;Time Setting</div>
        <div class="card-body"> 
          <form action=" " method="POST"> <!-- ------------------------FORM------------------------------ -->

            <div class="row">
              <div class="col-md-6 col-sm-6"> 
               <table class="customers_null">
                <tr>
                  <th>Time Source</th>
                  <td>
                    <input type="text" class="form-control"  placeholder="" id=""  name="" maxlength="">
                  </td>
                </tr>
                <tr>
                  <th>NTP Server</th>
                  <td>
                    <input type="text" class="form-control"  placeholder="" id=""  name="" maxlength="">
                  </td>
                </tr>
                <tr>
                  <th></th>
                  <td></td>
                </tr>
              </table>
            </div>

            <div class="col-md-6 col-sm-6"> 
             <table class="customers_null">
              <tr>
                <th>Set Date/Time</th>
                <td>
                  <div class="input-group-prepend"> 
                    <input type="text" class="form-control"  placeholder="" id="Date_Time"  name="" maxlength="">
                    <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                  </div>
                </td>
              </tr>
              <tr>
                <th></th>
                <td></td>
              </tr>
              <tr>
                <th></th>
                <td class="td r">
                  <button type="submit" class="btn btn-outline-success">Submit</button>
                </td>
              </tr>
            </table>
          </div>

        </div> 

      </form>
    </div>
  </div> 
</div>
</div>
<!-----------------------------TheEnd Row----------------------------->

<div class="row">
  <div class="col-md-12 col-sm-12"> 
    <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-fw fa-random"></i>&nbsp;Network Setting</div>
        <div class="card-body"> 
          <form action="database/db_Add_Network.php" method="POST"> <!-- ------------------------FORM------------------------------ -->

            <div class="row">
              <div class="col-md-6 col-sm-6"> 
               <table class="customers_null">
                <tr>
                  <th>IP Address</th>
                  <td>
                    <?php
                    $con = new connect();
                    $con->select_Network();
                    $result = $con->select_Network();
                    foreach ($result as $row) 
                      ?>
                    <?php
                    if(empty($row['id_network'])!=''){
                      ?>
                      <input type="text" class="form-control"  placeholder="" id="ip_address"  name="ip_address">
                      <?php
                    } 
                    else { ?>
                      <input type="text" class="form-control" placeholder="" id="ip_address" value="<?php echo $row['ip_address'];?>"  name="ip_address" disabled >
                    <?php } ?>
                  </td>
                </tr>
              </table>
            </div>
            <div class="col-md-6 col-sm-6"> 
             <table class="customers_null">
              <tr>
                <th>Subnet Mask</th>
                <td>
                  <?php
                  if(empty($row['id_network'])!=''){
                    ?>
                    <input type="text" class="form-control"  placeholder="" id="subnet_mask"  name="subnet_mask">
                    <?php
                  } 
                  else { ?>
                    <input type="text" class="form-control" placeholder="" id="subnet_mask" value="<?php echo $row['subnet_mask'];?>"  name="subnet_mask" disabled >
                  <?php } ?>
                </td>
              </tr>
              <tr>
                <th></th>
                <td class="td r">
                  <?php
                  if(empty($row['id_network'])!=''){
                    ?>
                    <button type="submit" class="btn btn-outline-success" id="submit_network">Submit</button>
                    <?php
                  } 
                  else { ?>
                    <a href="update_Setting.php"><button type="button" class="btn btn-outline-success"> Update </button></a>
                  <?php } ?>
                </td>
              </tr>
            </table>
          </div>
        </div> 
      </form>
    </div>
  </div> 
</div>
</div>
<!-----------------------------TheEnd Row----------------------------->

<div class="row">
  <div class="col-md-12 col-sm-12"> 
    <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-fw fa-wifi"></i>&nbsp;Wireless Setup (Station)</div>
        <div class="card-body"> 
          <form action=" " method="POST"> <!-- ------------------------FORM------------------------------ -->

            <div class="row">
              <div class="col-md-6 col-sm-6"> 
               <table class="customers_null">
                <tr>
                  <th>SSID</th>
                  <td>
                    <input type="text" class="form-control"  placeholder="" id=""  name="" maxlength="">
                  </td>
                </tr>
                <tr>
                  <th>IP Address</th>
                  <td>
                    <input type="text" class="form-control"  placeholder="" id=""  name="" maxlength="">
                  </td>
                </tr>
                <tr>
                  <th>Subnet Mask</th>
                  <td>
                    <input type="text" class="form-control"  placeholder="" id=""  name="" maxlength="">
                  </td>
                </tr>
                <tr>
                  <th>Default Gateway</th>
                  <td>
                    <input type="text" class="form-control"  placeholder="" id=""  name="" maxlength="">
                  </td>
                </tr>
              </table>
            </div>

            <div class="col-md-6 col-sm-6"> 
             <table class="customers_null">
              <tr>
                <th>Pass</th>
                <td>
                  <input type="text" class="form-control"  placeholder="" id=""  name="" maxlength="">
                </td>
              </tr>
              <tr>
                <th>Security</th>
                <td>
                  <select class="form-control" id="" name="">
                    <option selected>...</option>
                    <option value="">...</option>
                    <option value="">...</option>
                    <option value="">...</option>
                  </select>
                </td>
              </tr>
              <tr>
                <th>DHCP</th>
                <td>
                  <select class="form-control" id="" name="">
                    <option selected>...</option>
                    <option value="">...</option>
                    <option value="">...</option>
                    <option value="">...</option>
                  </select>
                </td>
              </tr>
              <tr>
                <th></th>
                <td></td>
              </tr>
              <tr>
                <th></th>
                <td class="td r">
                  <button type="submit" class="btn btn-outline-success">Submit</button>
                </td>
              </tr>
            </table>
          </div>

        </div> 

      </form>
    </div>
  </div> 
</div>
</div>
</div>
</div>
<!-----------------------------TheEnd Row----------------------------->






<!------------------------------------------------------Footer------------------------------------------------------>
<?php include("Footer.php");  ?>
