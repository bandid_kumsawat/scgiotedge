<?php
include 'config.php';


$arr = array();
$arr_put = array();

$i=0;

$sql = "SELECT * FROM processor";
// -------------------------------------------------------------------
$result = $conn->query($sql);

while ($row = $result->fetchArray(SQLITE3_ASSOC)){

  $arr[$i] = array(
    "id_processor"=>$row['id_processor'],
    "cpu_name"=>$row['cpu_name'],
    "temperature"=>$row['temperature'],
    "current_speed"=>$row['current_speed'],
    "load_average"=>$row['load_average']
  );
  $i++;
}


$conn->close();
$arr_put = array("Total"=>$i,"List"=>$arr);
echo (json_encode($arr_put)) ;

?>