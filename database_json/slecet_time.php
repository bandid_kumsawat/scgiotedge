<?php
include 'config.php';


$arr = array();
$arr_put = array();

$i=0;

$sql = "SELECT * FROM time_pi";
// -------------------------------------------------------------------
$result = $conn->query($sql);

while ($row = $result->fetchArray(SQLITE3_ASSOC)){

	$arr[$i] = array(
		"id_time"=>$row['id_time'],
		"day"=>$row['day'].'/'.$row['month'].'/'.$row['year'].' : '.$row['time'],
		"month"=>$row['month'],
		"year"=>$row['year'],
		"time"=>$row['time'],
		"time_zone"=>$row['time_zone']
	);
	$i++;
}


$conn->close();
$arr_put = array("Total"=>$i,"List"=>$arr);
echo (json_encode($arr_put)) ;

?>