<?php
include 'config.php';


$arr = array();
$arr_put = array();

$i=0;

$sql = "SELECT * FROM memory_usage";
// -------------------------------------------------------------------
$result = $conn->query($sql);

while ($row = $result->fetchArray(SQLITE3_ASSOC)){

  $arr[$i] = array(
    "id_total_memory"=>$row['id_total_memory'],
    "total_memory"=>$row['total_memory'],
    "used"=>$row['used'],
    "free"=>$row['free']
  );
  $i++;
}


$conn->close();
$arr_put = array("Total"=>$i,"List"=>$arr);
echo (json_encode($arr_put)) ;

?>