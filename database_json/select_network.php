<?php
include 'config.php';


$arr = array();
$arr_put = array();

$i=0;

$sql = "SELECT * FROM network";
// -------------------------------------------------------------------
$result = $conn->query($sql);

while ($row = $result->fetchArray(SQLITE3_ASSOC)){

  $arr[$i] = array(
    "id_network"=>$row['id_network'],
    "internet_status"=>$row['internet_status'],
    "ip_address"=>$row['ip_address'],
    "subnet_mask"=>$row['subnet_mask'],
    "dns"=>$row['dns']
  );
  $i++;
}


$conn->close();
$arr_put = array("Total"=>$i,"List"=>$arr);
echo (json_encode($arr_put)) ;

?>