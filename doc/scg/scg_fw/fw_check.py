import requests
import time
import sqlite3
import subprocess
import json
import commands
import os
app_path ="/root/scg_fw/app/"
web_path ="/root/scg_fw/web/"
sys_path ="/root/scg_fw/sys/"

node_path="/root/.node-red/start"
version_file = "/root/scg_fw/version.json"

def updateCurrentVer(last_ver):
    data = checkVersion()
    data['ITEM_VER'] = last_ver
    with open(version_file, 'w') as outfile:  
        json.dump(data, outfile)

def checkCurrentITEM():
    print "Check Current FW"
    
    con = sqlite3.connect("/var/www/html/database/EDGE_SYS.db")
    cursor = con.execute("SELECT * from tb_watt_cost LIMIT 1;")

    item = checkVersion()
    print item['ITEM']
    for data in cursor:
        print "ttt "+data[2]
        if(data[2] != item['ITEM']):
            print "not match"
            print "ReWrite ITEM"
            item['ITEM'] = data[2]
            item['ITEM_VER']=0
            writeNewITEM(item)
        return data[2]
    if con :
        con.close()

def writeNewITEM(data):
    with open(version_file, 'w') as outfile:  
        json.dump(data, outfile)

def checkVersion():
    with open(version_file) as json_file:  
        data = json.load(json_file)
        return data

def updateFW(url_fw):
    commands.getoutput("wget "+url_fw)
    fw = url_fw.split("/")
    file_n = fw[-1]

    # commands.getoutput("tar -xvzf "+file_n+" "+app_path)
    commands.getoutput("cp "+file_n+" "+app_path)
    print app_path + "run.sh"
    os.system(app_path +"run.sh")
    os.system("rm "+file_n)
    print "Update Finish"
    


def findURL(json_object, ver):
    for dict in json_object:
        if dict['ver'] == ver:
            return dict['url']

def checkFirmware():
    url = "http://128.199.247.187/scg_solar/fwlist.json"
    headers = {'cache-control': 'no-cache'}
    response = requests.request("GET", url, headers=headers)
    fw_json = json.loads(response.text)
    web_fw = fw_json[0]
    sys_fw = fw_json[1]
    item_fw = fw_json[2:]
    cur_item = checkCurrentITEM()
    print "\r\n## Current ITEM ##"
    print cur_item
    for data in item_fw:
        # print(data)
        if data['item_code'] == cur_item:
            print data
            print "OK!!!!"
            last_ver = data['last_version']
            print "Last ver"
            print last_ver
            tmp = checkVersion()
            cur_ver = tmp['ITEM_VER']
            print "Current ver"
            print cur_ver
            if last_ver != cur_ver:
                print "!!! Update FW !!!"
                url_up =  findURL(data['fw'],last_ver)
                print url_up
                updateFW(url_up)
                updateCurrentVer(last_ver)

            break





if __name__ == "__main__" :
    print "Check Firmware "
    checkVersion()
    try:
        checkFirmware()
    except :
        print "Error"
    
