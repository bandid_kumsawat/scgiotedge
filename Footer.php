




<!-- footer Modal-->
<footer class="sticky-footer" >
  <div class="container">
    <div class="text-center">
      <small> © SCG Cement-Building Materials Co., Ltd. </small>
    </div>
  </div>
</footer>
<!-- footer Modal-->

<!-- Logout Modal-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Logout </h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Do you want to sign out ?</div>
      <div class="modal-footer">
        <a class="btn btn-primary" href="logout.php">Logout</a>
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Logout Modal-->



<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>
<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>
<!-- Custom scripts for this page-->
<script src="js/sb-admin-datatables.min.js"></script>

<script src="js/jquery.mask.js"></script>

<script language="JavaScript">

 $('#Date_Time').mask('00/00/0000 00:00:00'); 

 $('#ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {translation:  {'Z': {pattern: /[0-9]/, optional: true}}});
 $('#subnet_mask').mask('0ZZ.0ZZ.0ZZ.0ZZ', {translation:  {'Z': {pattern: /[0-9]/, optional: true}}});

 $(document).ready(function(){
    wifi_connected();
    wifi_scan_show();
    first_load_device();
    scg_set_url();
    reset_modal();
    create_modal();
    reset_modal1();
    update_device();
    setInterval(function(){ wifi_connected();  },1000);
    //scg_select_device();
   $("#submit_network").click(function(){
     var ip_address = $("#ip_address").val().length;
     if (ip_address <= 8 ){
       alert('กรุณาใส่เลข IP ให้ครบถ้วน'); 
       return false;
     }
       }); //The End function
   $("#submit_network").click(function(){
     var subnet_mask = $("#subnet_mask").val().length;
     if (subnet_mask <= 8 ){
       alert('กรุณาใส่เลข Subnet Mask ให้ครบถ้วน') ; 
       return false;
     }
       }); //The End function


});
 

// -------------------------------------------------------------- SCG ------------------------------------------------------------------ //

var id_scg = "";
function scg_set_url(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "database/scg_url.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "9205b06a-8e42-8087-a2ac-0b2691644312"
    }
  }
  $.ajax(settings).done(function (response) {
    var data = JSON.parse(response);
    console.log(data);


    if(data.List[1].url_broker == 0){
      //$("#mqtt").val("-");
    }else{
      $("#mqtt-name").val(data.List[1].url_broker);
      // $("#mqtt-name").val(data.List[1].url_broker.split("//")[1].split(":")[0]);
	    // $("#mqtt-port").val(data.List[1].url_broker.split("//")[1].split(":")[1]);
      // console.log($("#mqtt-port").val(data.List[1].url_broker.split("//")[1].split(":")[1]))
    }
	
	
    if(data.List[1].url_iot_platform == 0){
      //$("#iot").val("-");
    }else{
      $("#iot-name").val(data.List[1].url_iot_platform);
      // $("#iot-name").val(data.List[1].url_iot_platform.split("//")[1].split(":")[0]);
	    // $("#iot-port").val(data.List[1].url_iot_platform.split("//")[1].split(":")[1].split("/")[0]);
    }
	
	
    if(data.List[1].url_mobile == 0){
      //$("#device").val("-");
    }else{
      $("#device-name").val(data.List[1].url_graphql);
	  // $("#device-name").val(data.List[1].url_graphql.split("//")[1].split(":")[0]);
	  // $("#device-port").val(data.List[1].url_graphql.split("//")[1].split(":")[1]);  
    }
	
	
    if(data.List[1].url_graphql == 0){
      //$("#url_g").val("-");
    }else{
      $("#url_g-name").val(data.List[1].url_mobile);
      // $("#url_g-name").val(data.List[1].url_mobile.split("//")[1]);
    }


    // Enroll Device
    if(data.List[1].tag == 0){
      $("#tag").val("-");
    }else{
      $("#tag").val(data.List[1].tag);
    }
    id_scg = data.List[1].id_scg;
  });
}


/*

Modal Create here.

*/

function reset_modal(){
        var modal = document.getElementById("modal_content");

        modal.innerHTML=
            '<div class="modal-header" id="modal_header">'+
            '</div>'+
            '<div class="row modal-body" id="modal_body">'+
            '</div>'+
            '<div class="modal-footer" id="modal_footer">'+
            '</div>';
  }


  function reset_modal(){
        var modal = document.getElementById("modal_content");

        modal.innerHTML=
            '<div class="modal-header" id="modal_header">'+
            '</div>'+
            '<div class="row modal-body" id="modal_body">'+
            '</div>'+
            '<div class="modal-footer" id="modal_footer">'+
            '</div>';
  }  
  function reset_modal1(){
        var modal = document.getElementById("modal_content_endlow");

        modal.innerHTML=
            '<div class="modal-header" id="modal_header1">'+
            '</div>'+
            '<div class="row modal-body" id="modal_body1">'+
            '</div>'+
            '<div class="modal-footer" id="modal_footer1">'+
            '</div>';
  }

  function create_modal(){
    $( "#submit_click" ).click(function() {

      reset_modal();

      $('#modal_header').append(
        '<h5 class="modal-title"><i class="far fa-images"></i>Confirm</h5>'+
        '<button type="button" class="close" data-dismiss="modal">&times;</button>'
      );

          $('#modal_body').append(
            '<div class="container-fluid">'+
              // 'MQTT Server <input type="text"  class="form-control" name="ip_address" id="Show_iot" value="tcp://'+		$("#mqtt-name").val()+":"+$("#mqtt-port").val()			+'" >'+
              // 'IoT Server <input type="text"  class="form-control" name="subnet_mask" id="Show_api" value="https://'+		$("#iot-name").val()+":"+$("#iot-port").val()+"/oauth/token"			+'">'+
              // 'Device Server <input type="text"  class="form-control" name="mode" id="Show_dev" value="https://'+			$("#url_g-name").val()								+'">'+ 
              // 'URL Graphql <input type="text"  class="form-control" name="mode" id="Show_url_gra" value="https://'+			$("#device-name").val()+":"+$("#device-port").val()		+'">'+ 
              'MQTT Server <input type="text"  class="form-control" name="ip_address" id="Show_iot" value="' + $("#mqtt-name").val()+'" >'+
              'IoT Server <input type="text"  class="form-control" name="subnet_mask" id="Show_api" value="'+		$("#iot-name").val()+'">'+
              'Device Server <input type="text"  class="form-control" name="mode" id="Show_dev" value="'+			$("#url_g-name").val()								+'">'+ 
              'URL Graphql <input type="text"  class="form-control" name="mode" id="Show_url_gra" value="'+			$("#device-name").val()+'">'+ 
            '</div>'
          );

      $("#modal_footer").append(
        '<button type="submit" class="btn btn-success" onclick = "click_btn()">Save</button>'
      );
    });
    $( "#submit_click_end_low" ).click(function() {

      reset_modal1();

      $('#modal_header1').append(
        '<h5 class="modal-title"><i class="far fa-images"></i>Confirm Enroll Device </h5>'+
        '<button type="button" class="close" data-dismiss="modal">&times;</button>'
      );

      $('#modal_body1').append(
        '<div class="container-fluid">'+
          'Username <input type="text"  class="form-control" name="ip_address" id="Show_username" >'+
          'Password <input type="password"  class="form-control" name="ip_address" id="Show_password"  >'+
          'TAG <input type="text"  class="form-control" name="ip_address" id="Show_tag" value="'+$("#tag").val()+'" >'+
          //'Client ID Server <input type="text"  class="form-control"  disabled="disabled" name="subnet_mask" id="Show_client_id" value="'+$("#cid").val()+'">'+
        '</div>'
      );

      $("#modal_footer1").append(
        '<button type="submit" class="btn btn-success" onclick = "click_btn_end_low()">Save</button>'
      );
    });
  }
/*
  Detect Event Modal Click Here.
*/
  function click_btn(){
    var Show_iot = $("#Show_iot").val();
    var Show_api = $("#Show_api").val();
    var Show_dev = $("#Show_dev").val();
    var Show_url_gra = $("#Show_url_gra").val();
    if (Show_iot == "-" || Show_iot == "" || Show_api == "-" || Show_api == "" || Show_dev == "-" || Show_dev == "" || Show_url_gra == "-" || Show_url_gra == "" ){
      $("#modalscg").modal("hide");
      alert("กรุณากรอกข้อมูลให้ครบ");
    }else{
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "database/scg_update_url.php?Show_iot="+Show_iot+"&Show_api="+Show_api+"&Show_dev="+Show_dev+"&Show_url_gra="+Show_url_gra,
        "method": "GET",
        "headers": {
          "cache-control": "no-cache",
          "postman-token": "150fccdd-6e49-3d73-b503-f982769ff64d"
        }
      }
      $.ajax(settings).done(function (response) {
        var data = JSON.parse(response);
        console.log("-- Save1 on SQLite3 --");
        console.log(data);
      });
    }
    /* restart service */
    restart_service();
  }

  function click_btn_end_low(){
    var user = $("#Show_username").val();
    var pass = $("#Show_password").val();
    var tag = $("#Show_tag").val();
    if (user == "-" || user == "" || pass == "-" || pass == "" || tag == "-" || tag == ""){
      $("#modalend_low").modal("hide");
      alert("กรุณากรอกข้อมูลให้ครบ");
    }else{
      // api p'deaw
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "database/scg_update_up_and_tag.php?username="+user+"&password="+pass+"&tag="+tag,
        "method": "GET",
        "headers": {
          "cache-control": "no-cache",
          "postman-token": "150fccdd-6e49-3d73-b503-f982769ff64d"
        }
      }
      $.ajax(settings).done(function (response) {
        var data = JSON.parse(response);
        console.log("-- Save2 on SQLite3 --");
        console.log(data);
      });


      /* restart service */
      restart_service();
      
      //window.location.replace("update_Setting.php");
    }
  }
  function client_id(){
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "database/scg_get_client_id.php",
        "method": "GET",
        "headers": {
          "cache-control": "no-cache",
          "postman-token": "150fccdd-6e49-3d73-b503-f982769ff64d"
        }
      }
      $.ajax(settings).done(function (response) {
        var data = JSON.parse(response);
        console.log(data);
        if (data.List[1].client_id == 0)
          $("#cid").val("-");
        else
          $("#cid").val(data.List[1].client_id);
      });
  }

  function restart_service(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "database/scg_restart_service.php",
      "method": "GET"
    }
    $.ajax(settings).done(function (response) {
      try {
        var data = JSON.parse(response);
        console.log(data);
        window.location.replace("update_Setting.php");
      }catch(err) {
        console.log(err);
        alert("ERROR");
      }
    });
  }



  function update_device(){
    // scg_device_select update_device

    $("#update_device").click(function(){

      if ($("#watt").val() != 0){
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "database/scg_insert_device_wat_to_cost.php?device_id="+$("#scg_device_select").val()+"&watt_cost="+$("#watt").val(),
          "method": "GET",
          "headers": {
            "cache-control": "no-cache"
          }
        }
        $.ajax(settings).done(function (response) {
          console.log(response);
          var data = JSON.parse(response);
          if (data.status == "OK"){
            alert ("Select Invverter [ OK ]");
          }
        }); 
        


      }else{
        alert("Input Watt");
      }

    });

  }
  function first_load_device(){

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "api_oneweb/json/scg_get_json_model.php",
      "method": "GET",
      "headers": {
        "cache-control": "no-cache",
        "postman-token": "150fccdd-6e49-3d73-b503-f982769ff64d"
      }
    }
    
    $.ajax(settings).done(function (response) {
      var data = JSON.parse(response);
      for (var i = 0;i < data.length;i++){
        $("#scg_device_select").append(
          '<option value="'+data[i].item_code+'">'+data[i].item_display+'</option>'
        );
      }
    });

  }

  function wifi_scan_show(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "./database_json/select_wifi_name.php",
      "method": "GET",
      "headers": {
        "cache-control": "no-cache",
      }
    }
    
    $.ajax(settings).done(function (response) {
      var data = JSON.parse(response);
      console.log(data);
      for (var i = 0;i < data.List.length;i++){
        $("#scg_device_select_wifi").append(
          '<option value="'+data.List[i].ssid+'">'+data.List[i].ssid+'</option>'
        );
      }
    });
  }
  function scg_save_wifi(){
    var ssid = $("#scg_device_select_wifi").val();
    var pass = $("#wifi_ssid").val();
    console.log(ssid + " " + pass);


    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "./database_json/scg_save_to_wifi_db.php?ssid="+ssid+"&pass="+pass,
      "method": "GET",
      "headers": {
        "cache-control": "no-cache",
      }
    }
    
    $.ajax(settings).done(function (response) {
      var data = JSON.parse(response);
      console.log(response);
      if (data.status == "OK"){
        alert("Successful")
      }
    });
  }


  function wifi_connected(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "./database_json/wifi_connected.php",
      "method": "GET",
      "headers": {
        "cache-control": "no-cache",
      }
    }
    
    $.ajax(settings).done(function (response) {
      var data = JSON.parse(response);
      console.log(response);
      $("#wifi_name_show").text(data.List[0].ssid);
      $("#wifi_pass_show").text(data.List[0].pass);
    });
  }

  function scg_update_up(id){
    var user = $("#username_up").val();
    var pass = $("#password_up").val();
    if (user != "" && pass != ""){
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "./database/scg_update_admin.php?username="+user+"&password_=" + pass + "&id=" + id,
        "method": "GET",
        "headers": {
          "cache-control": "no-cache",
        }
      }
      
      $.ajax(settings).done(function (response) {
        console.log(response)
        var data = JSON.parse(response);
        console.log(response);
        if (data.status){
          alert("Successful")
        }
      });
    }
}
</script>

</body>

</html>
