<?php
session_start();
include_once("database/connect.php");
$con = new connect();
$con->select_User();
$result = $con->select_User();
// foreach ($result as $row) 
// if($_SESSION['user'] !=  $row['user']  ){
//   header('Location: login.php');
// }
if($_SESSION['type'] == "admin" || $_SESSION['type'] == 'user'){
}else {
  header('Location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Deaware</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
  <link href="css/css.css" rel="stylesheet">

  <script src="js/jquery-3.3.1.js"></script>
  
<!-- <style>
@import url('https://fonts.googleapis.com/css?family=Arimo');
</style> -->

</head>

<body class="fixed-nav sticky-footer" id="page-top"   style="background-color: #000405;"   >    <!-- bg-dark -->
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav"  style="background-color: #000405;"  ><!--   bg-dark  -->
   <!--   <img src="img/Deaware.png" width="35" height="author"> -----------------------------------------------------   -->
   &nbsp;&nbsp;<img src="img/dwlogo_1.png" width="170" height="author"> 
   <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarResponsive"   >
    <ul class="navbar-nav navbar-sidenav" id="exampleAccordion"   style="background-color: #000405;"  >   
     <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables"  >
     </li>

     <li class="Dashboard nav-item " data-toggle="tooltip" data-placement="right" title="Dashboard"   >
      <a class="nav-link" href="dashboard.php">
        &nbsp;
        <i class="fa fa-fw fa-calendar-o" aria-hidden="true"></i>
        <span class="nav-link-text">Dashboard</span>
      </a>
    </li>
<!--
    <li class="Connection nav-item" data-toggle="tooltip" data-placement="right" title="Tables"  >
      <a class="nav-link" href="connection.php">
        &nbsp;
        <i class="fa fa-fw fa-home"></i>
        <span class="nav-link-text" >Connection</span>
      </a>
    </li>
-->
 <!--    <li class="Setting nav-item" data-toggle="tooltip" data-placement="right" title="Tables"  >
      <a class="nav-link" href="setting.php">
        &nbsp;
        <i class="fa fa-fw fa-cogs"></i>
        <span class="nav-link-text" >Setting</span>
      </a>
    </li> -->



  <?php if ($_SESSION['type'] == 'admin'){ ?>
    <li class="Setting nav-item" data-toggle="tooltip" data-placement="right" title="Components" >
      <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
        &nbsp;
        <i class="fa fa-fw fa-cogs"></i>
        <span  class="nav-link-text"> Setting</span>
      </a>
      <ul class="sidenav-second-level collapse" id="collapseComponents" style="background-color: #000">
        <!-- <li>
          <a href="add_Setting.php">Add Setting</a>
        </li> -->
        <li>
          <a href="update_Setting.php">Update Setting</a>
        </li>
        <!-- <li>
          <a href="Account_Management.php">Account Management</a>
        </li> -->

      </ul>
    </li>


<?php } ?>

    <!-- <li class="manual nav-item" data-toggle="tooltip" data-placement="right" title="Tables"  >
      <a class="nav-link" href="manual.php">
        &nbsp;
        <i class="fa fa-fw fa-file-pdf-o"></i>
        <span class="nav-link-text" >Manual</span>
      </a>
    </li> -->
    <!-- Navigation-->
  </ul>

  <ul class="navbar-nav sidenav-toggler">

  </ul>
  <ul class="navbar-nav ml-auto">
    <li class="nav-item">
      <form class="form-inline my-2 my-lg-0 mr-lg-2">
        <div class="input-group">
          <a class="nav-link"  id="toggleNavColor">
            <i class="fa fa-fw fa-user" style="color: #fff"></i>
            <span class="nav-link-text" style="color: #fff"> <?php echo $_SESSION['type'];?> </span>
          </a> 
        </div>
      </form>
    </li>

    <li class="nav-item">
     <a class="nav-link" data-toggle="modal" data-target="#exampleModal" style="color: #fff" >
      <i class="fa fa-fw fa-sign-out"></i>Logout</a>
    </li>
  </ul>
</div>
</nav>
<!-- Navigation-->

<div class="content-wrapper">
  <div class="container-fluid">


