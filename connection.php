<?php
include("Header.php");  
?>
<style>
.Connection{ 
  background-color: #CE0600;
}
</style>
<!------------------------------------------------------Header------------------------------------------------------>

<div class="row"> 
  <div class="col-md-12 col-sm-12"> 
    <div class="alert" style="background-color: #CE0600; color: #fff;"  role="alert">
      <i class="fa fa-fw fa-home" aria-hidden="true"></i>
      Connection 
    </div>
  </div>

</div>

<div class="row"> 

  <div class="col-md-12 col-sm-12"> 
    <div class="card mb-3" >
      <div class="card-header">
        <i class="fa fa-fw fa-random"></i> Lan Status</div>
        <div class="card-body">

          <div class="row">

            <div class="col-12 col-md-6">
             <table class="customers">
              <tr>
                <th>Connection Status</th>
                <td id="internet_status"></td>
                <tr>
                  <th>IP Address</th>
                  <td id="ip_address"></td>
                </tr>
                <tr>
                  <th>Subnet</th>
                  <td id="subnet_mask"></td>
                </tr>
              </table>
            </div>

            <div class="col-12 col-md-6">
             <table class="customers">
              <tr>
                <th>Connection Type </th>
                <td>LAN</td>
                
                </tr>
              </table>
            </div>

          </div>

        </div>
      </div>
    </div>

  </div>
  
  <!-- <div class="row"> 

    <div class="col-md-12 col-sm-12"> 
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-fw fa-wifi"></i> WLAN Status</div>
          <div class="card-body">

           <div class="row">

            <div class="col-12 col-md-6">
             <table class="customers">
              <tr>
                <th>Wifi Mode</th>
                <td>Null</td>
                <tr>
                  <th>Connection Status</th>
                  <td>Null</td>
                </tr>
                <tr>
                  <th>IP Address</td>
                    <td>Null</td>
                  </tr>
                  <tr>
                    <th>Subnet</th>
                    <td>Null</td>
                  </tr>
                </table>
              </div>

              <div class="col-12 col-md-6">
               <table class="customers">
                <tr>
                  <th>Connection Type</th>
                  <td>Null</td>
                  <tr>
                    <th>Wifi SSID</th>
                    <td>Null</td>
                  </tr>
                  <tr>
                    <th>DNS</th>
                    <td>Null</td>
                  </tr>
                </table>
              </div>

            </div>
          </div>
        </div>
      </div>

    </div> -->
    

    



<script language="JavaScript" >
  
  var time =1;

  $( document ).ready(function() {
    setInterval(function(){ select_network();  }, time*1000);
  });

function select_network(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "database_json/select_network.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "fd23168e-aeef-03a5-1e40-110305d16df8"
    }
  }
  $.ajax(settings).done(function (response) {
    console.log(response);
    var select_network = JSON.parse(response);

    // $('#id_network').text(select_network.List[0].id_network);
    // $('#internet_status').text(select_network.List[0].internet_status);
    // $('#ip_address').text(select_network.List[0].ip_address);
    // $('#subnet_mask').text(select_network.List[0].subnet_mask);
    // $('#dns').text(select_network.List[0].dns);

    if($('#id_network').text() == ""){
      $('#id_network').text("Null");
    }else{
      $('#id_network').text(select_network.List[0].id_network);
    }
    if($('#internet_status').text() == ""){
      $('#internet_status').text("Null");
    }else{
      $('#internet_status').text(select_network.List[0].internet_status);
    }
    if($('#ip_address').text() == ""){
      $('#ip_address').text("Null");
    }else{
      $('#ip_address').text(select_network.List[0].ip_address);
    }
    if($('#subnet_mask').text() == ""){
      $('#subnet_mask').text("Null");
    }else{
      $('#subnet_mask').text(select_network.List[0].subnet_mask);
    }
    if($('#dns').text() == ""){
      $('#dns').text("Null");
    }else{
      $('#dns').text(select_network.List[0].dns);
    }
  });
}

</script>

<?php include("Footer.php");  ?>