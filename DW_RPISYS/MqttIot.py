import sqlite3 as lite
import sys
from time import gmtime, strftime
import commands
import json
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt


class MQttIot:
  def __init__(self):
    self.iot_client = mqtt.Client()


    def on_connect(client, userdata, flags, rc):
        # global mqtt_conn
        mqtt_conn = 1
        # print ("\r\n\r\nConnected MQTT Local with result code: %s" % rc)
        # client.subscribe('app/#');

    def on_disconnect(client, userdata, rc):
        # global mqtt_conn
        # mqtt_conn = 0
        # print ("\r\n Disconnected Local Broker with result code: %s" % rc)

    def on_message(client, userdata, msg):
        payload  = msg.payload
        # try:
        #     data = json.loads(payload)
        #     # Send Message to Azure
        #     if(data['event']):
        #         data['serial']=serial
        #         print ("Payload : " + str(data))
        #         print (data['serial'])
        #         pub_data(json.dumps(data),azure_topic)


        #     if(data['SUBSYS_NAME']=='GW'):
        #         print ('Gateway Message')

        # except Exception as e:
        #     print (e)

    def on_publish(client, userdata, mid):
        print ('\r\n!!!!!!!! publish !!!!!!!!')
        # print("Message {0} sent from {1}".format(str(mid), deviceId))


    def connect() :
        info = getServerInfo()
        info_json = json.loads(info)
        print info_json

    def getServerInfo() :
        # con = lite.connect("../database/rpi_sys.db")   
        con = lite.connect("C:/xampp/htdocs/scg_invertor/database/rpi_sys.db")
        try:
            with con:
                cur = con.cursor()
                cur.execute("SELECT * from solar_scg")
                for row in cur:
                    # print row
                    ret = {  
                        'url_iot':row[1],
                        'url_mobile':row[2],
                        'url_graphql':row[3],
                        'url_broker':row[4],
                        'url_iot':row[4],
                    }
            con.close()
        
            return json.dumps(ret)
        except err as e:
            print e
            ret = {  
                'url_iot':"",
                'url_mobile':"",
                'url_graphql':"",
                'url_broker':"",
                'url_iot':"",
            }
            return json.dumps(ret)
   

	# if con :
	# 	cur.close()
# broker_address="192.168.1.184" 
# #broker_address="iot.eclipse.org" #use external broker


# client = mqtt.Client("P1") #create new instance
# client.connect(broker_address) #connect to broker
# client.publish("house/main-light","OFF")#publish


    if __name__ == "__main__" :
        print "### mqtt iot connect ###"
        # a = getServerInfo()
        # b = json.loads(a)
        # print b
        connect()

    