import sqlite3
conn = sqlite3.connect("/dev/shm/rpi_sys.db")


c = conn.cursor()

# Create table
c.execute('''CREATE TABLE disk_usage
             (id_disk_usage	INTEGER ,
            total_disk     text,
             used           text,
              free          text,
              PRIMARY KEY(id_disk_usage) )''')

c.execute('''CREATE TABLE general
             (id_general        INTEGER ,
             name               text,
             serial_numbe       text,
             product_name       text,
             product_version    text,
             operating_system   text,
             software_version   text,
             PRIMARY KEY('id_general'))''')

c.execute('''CREATE TABLE memory_usage
             (id_total_memory    INTEGER ,
             total_memory        text,
             used                text,
	     free                text,
             PRIMARY KEY('id_total_memory'))''')

c.execute('''CREATE TABLE network
             (id_network            INTEGER ,
             internet_status        text,
             ip_address             text,
             subnet_mask            text,
             dns                    text,
             PRIMARY KEY('id_network'))''')

c.execute('''CREATE TABLE processor
             (id_processor           INTEGER,
             cpu_name                text,
             temperature             text,
             current_speed           text,
             load_average            text,
             PRIMARY KEY('id_processor'))''')

c.execute('''CREATE TABLE time_pi
             (id_time                 INTEGER,
             day                      text,
             month                    text,
             year                     text,
             time                     text,
             time_zone                text,
             PRIMARY KEY('id_time'))''')

c.execute('''CREATE TABLE user
             (user_id                 text,
             pass                     text,

             PRIMARY KEY('user_id') )''')

# Insert a row of data
c.execute("INSERT INTO disk_usage VALUES ('1','2','3','4')")

c.execute("INSERT INTO general VALUES ('1','2','3','4','5','6','7')")

c.execute("INSERT INTO memory_usage VALUES ('1','2','3')")

c.execute("INSERT INTO network VALUES ('1','2','3','4','5')")

c.execute("INSERT INTO processor VALUES ('1','2','3','4','5')")

c.execute("INSERT INTO time_pi VALUES ('1','2','3','4','5','6')")

c.execute("INSERT INTO user VALUES ('1','2')")
# Select  data

with conn:
#        cur = conn.cursor()
	c.execute("select * from general")
	rows = c.fetchall()


#for row_disk in s_disk:
 #   print "Disk ::",row_disk

	for row in rows:
		    print "General :: ",row

#for row_mem in s_mem:
 #   print "Memory ::",row_mem

#for row_net in s_network:
#    print "Network ::",row_net





# Save (commit) the changes
print " Suscesfuly "
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
