import sqlite3 as lite
import sys
from time import gmtime, strftime
import commands
import socket

#############  READ API : Raspberry PI Hardware ########################



def getOperating_system() :
	os1 = commands.getoutput("hostnamectl |grep 'System'| awk -F'[: ]+' '{ print $4}'")
	os2 = commands.getoutput("hostnamectl |grep 'System'| awk -F'[: ]+' '{ print $5}'")
	os3 = commands.getoutput("hostnamectl |grep 'System'| awk -F'[: ]+' '{ print $6}'")
	os4 = commands.getoutput("hostnamectl |grep 'System'| awk -F'[: ]+' '{ print $7}'")
	null = " "
	OS  = os1+null+os2+null+os3+null+os4
	return OS

def getHostname() :
	return commands.getoutput("hostname")

def getNamewifi() :
	return commands.getoutput('''iwconfig wlan0 |grep 'ESSID:' |awk -F '["]+' '{print $2}' ''')

def getVersion_pi() :
	vs1 = commands.getoutput("hostnamectl |grep 'Kernel'| awk -F'[: ]+' '{ print $3}'")
	vs2 = commands.getoutput("hostnamectl |grep 'Kernel'| awk -F'[: ]+' '{ print $4}'")
	null = " "
	VS = vs1+null+vs2
	return VS


def getGateway() :
	# commands.getoutput("")
	null="geteway"
	return null

def getIPAddress() :
	return commands.getoutput("ifconfig eth0 | grep 'inet ' | awk -F'[: ]+' '{ print $3 }'")

def getIPAddress1() :
	return commands.getoutput("ifconfig eth1 | grep 'inet ' | awk -F'[: ]+' '{ print $4 }'")

def getNetMask() :
	return commands.getoutput("ifconfig eth0 | awk '/netmask/{print $4}'")

def getNetMask1() :
	# return commands.getoutput("ifconfig eth1 | awk '/netmask/{print $4}'")
	return commands.getoutput("ifconfig eth1 | grep 'inet ' | awk -F'[: ]+' '{ print $8 }'")
	
	 

def getIPWiFi() :
	return commands.getoutput("ifconfig wlan0 | grep 'inet ' | awk -F'[: ]+' '{ print $3 }'")

def getNetmaskWifi() :
	return commands.getoutput("ifconfig wlan0 | awk '/netmask/{print $4}'")

####### disk_usage #######
def getDiskTotal() :
	# return commands.getoutput("df -h |grep '/dev/root'| awk -F'[: ]+' '{ print $2 }'")
	return commands.getoutput("df -h |grep '/dev/mmcblk0p2'| awk -F'[: ]+' '{ print $2 }'")

def getDiskUsed() :
	# return commands.getoutput("df -h |grep '/dev/root'| awk -F'[: ]+' '{ print $3 }'")
	return commands.getoutput("df -h |grep '/dev/mmcblk0p2'| awk -F'[: ]+' '{ print $3 }'")

def getDiskFree() :
	# return commands.getoutput("df -h |grep '/dev/root'| awk -F'[: ]+' '{ print $4 }'")
	return commands.getoutput("df -h |grep '/dev/mmcblk0p2'| awk -F'[: ]+' '{ print $4 }'")

##########################

########## getMem ########

def getMemTotal() :
        return commands.getoutput("free -h |grep 'Mem'| awk -F'[: ]+' '{ print $2 }'")

def getMemUsed() :
        return commands.getoutput("free -h |grep 'Mem'| awk -F'[: ]+' '{ print $3 }'")

def getMemFree() :
        return commands.getoutput("free -h |grep 'Mem'| awk -F'[: ]+' '{ print $4 }'")
######################

def getTemperature() :
	return commands.getoutput('''vcgencmd measure_temp|awk -F '=' '{print $2}'|awk -F "'"   '{print $1}' ''')

def getUname() :
	return commands.getoutput('uname')

def getDNS() :
	return commands.getoutput("cat /etc/resolv.conf |grep 'nameserver' | awk -F '[ ]+' '{print $2}'")

def getLoadAverage() :
	return commands.getoutput("uptime |grep 'average'| awk -F'[: ]+' '{ print $12 " " $13 " " $14 " " }'")

def getCPUNAME():
	cpu1 = commands.getoutput("lscpu|grep 'name'| awk -F'[: ]+' '{ print $3}'")
	cpu2 = commands.getoutput("lscpu|grep 'name'| awk -F'[: ]+' '{ print $4}'")
	cpu3 = commands.getoutput("lscpu|grep 'name'| awk -F'[: ]+' '{ print $5}'")
	cpu4 = commands.getoutput("lscpu|grep 'name'| awk -F'[: ]+' '{ print $6}'")
	cpu5 = commands.getoutput("lscpu|grep 'name'| awk -F'[: ]+' '{ print $7}'")
	null = " "
	CUP = cpu1+null+cpu2+null+cpu3+null+cpu4+null+cpu5
	return CUP


def getkernel():
        return commands.getoutput('lsb_release -d')


def getTime():
	return commands.getoutput("uptime |grep 'average'| awk -F'[ ]+' '{ print $2}'")

def getYear():
	return strftime("%Y", gmtime())

def getMonth():
        return strftime("%B", gmtime())

def getDay():
        return strftime("%A", gmtime())

def getTime_Zone():
        return strftime("%Z", gmtime())

def getcurren_speed():
	MHZ= commands.getoutput('cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq')
	str=int(MHZ)
	curren=(str/1000)
	return curren



#############  WRITE API : Raspberry PI Hardware #######################
def writeRPIInf() :

	con = lite.connect("/dev/shm/rpi_sys.db")
	with con :
		cur  = con.cursor()
		#### TODO : Using Real Value from function to set data
		# ETH0
		# cur.execute("UPDATE network SET ip_address=?, subnet_mask= ?, dns=?, ip_address_wifi=?, subnet_mask_wifi=?,name_wifi =?,gateway =?,hostname =? ;", [getIPAddress(), getNetMask(),getDNS(),getIPWiFi(),getNetmaskWifi(),getNamewifi(),getGateway(),getHostname()])

		# ETH1
		cur.execute("UPDATE network SET ip_address=?, subnet_mask= ?, dns=?, ip_address_wifi=?, subnet_mask_wifi=?,name_wifi =?,gateway =?,hostname =? ;", [getIPAddress1(), getNetMask1(),getDNS(),getIPWiFi(),getNetmaskWifi(),getNamewifi(),getGateway(),getHostname()])

		cur.execute("UPDATE  disk_usage SET total_disk=?,used = ?,free=?;",[getDiskTotal(),getDiskUsed(),getDiskFree()])

		cur.execute("UPDATE  memory_usage SET total_memory=?,used=?,free=?;",[getMemTotal(),getMemUsed(),getMemFree()])

		cur.execute("UPDATE  processor SET cpu_name=?,temperature=?,current_speed=?,load_average=?;",[getCPUNAME(),getTemperature(),getcurren_speed(),getLoadAverage()])

		cur.execute("UPDATE  time_pi SET day=?,month=?,year=?,time=?,time_zone=?;",[getDay(),getMonth(),getYear(),getTime(),getTime_Zone()])

	        cur.execute("UPDATE  general SET operating_system=?, software_version=?;",[getOperating_system(),getVersion_pi()])

	if con :
		cur.close()



if __name__ == "__main__" :
	print "RPI INTERFACE UNITTEST"
	writeRPIInf()
