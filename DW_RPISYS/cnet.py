import sys
import os
import sqlite3 as lite
def checkInterNet(S) :
    rep = os.system('ping -c1 ' + S)

    con = lite.connect('/dev/shm/rpi_sys.db')
    with con:
        cur = con.cursor()
	if rep == 0 :
		print "[CheckInternet] Successfully PING " + S
       		cur.execute("UPDATE network SET internet_status= ?;", ['ON'])

	else :
		print "[CheckInternet] Failed PING " + S
	        cur.execute("UPDATE network SET internet_status= ?;", ['OFF'])

    if con :
	cur.close()


    return rep


if __name__ == "__main__" :
    print "UNITTEST CHECK INTERNET MODULE"
    print checkInterNet("www.google.com")
