<?php
session_start();
echo $_SESSION['type'];
if($_SESSION['type'] == "admin"){
}else {
    header('Location: login.php');
}
?>
<?php
  include("Header.php");
  // include("database/connect.php");
?>
<?php
if(isset($_POST['Refresh']))
{
exec("python /var/www/html/api_oneweb/main.py");
}
?>
<?php
Function config_set($config_file, $section, $key, $value) {
    $config_data = parse_ini_file($config_file, true, INI_SCANNER_RAW);
    if (empty($config_data[$section][$key])) {
        $config_data[$section][$key] = $value;
    } else {
        $config_data[$section][$key] = ' '.$value;
    }
    $new_content = '';
    foreach ($config_data as $section => $section_content) {
        $section_content = array_map(function($value, $key) {
            return "$key=$value";
        }, array_values($section_content), array_keys($section_content));
        $section_content = implode("\n", $section_content);
        $new_content .= "[$section]\n$section_content\n";
    }
    file_put_contents($config_file, $new_content);
}
if(isset($_POST['Enroll']))
{
$tagid = $_POST['TagID'];
config_set ('./frp/frpc_scgcbm.ini','common','server_addr','112.121.150.167');
config_set ('./frp/frpc_scgcbm.ini','common','user','scg_'.$tagid);
exec('./phpfrprestart');
}
?>
<?php
if(isset($_POST['Reboot']))
{
exec('./phpreboot');
}
?>
<style>
  .Setting{
    background-color: #CE0600;
  }
</style>
<!------------------------------------------------------Header------------------------------------------------------>
<div class="row">
  <div class="col-md-12 col-sm-12">
    <div class="alert" style="background-color: #CE0600; color: #fff;"  role="alert">
      <i class="fa fa-fw fa-cog" aria-hidden="true"></i>
      Update Setting
    </div>
  </div>
</div>

<!-- <div class="row">
  <div class="col-md-12 col-sm-12">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-hourglass"></i>&nbsp;Time Setting</div>
        <div class="card-body">
          <form action=" " method="POST">
            <div class="row">
              <div class="col-md-6 col-sm-6">
               <table class="customers_null">
                <tr>
                  <th>Time Source</th>
                  <td>
                    <input type="text" class="form-control"  placeholder="" id=""  name="" maxlength="">
                  </td>
                </tr>
                <tr>
                  <th>NTP Server</th>
                  <td>
                    <input type="text" class="form-control"  placeholder="" id=""  name="" maxlength="">
                  </td>
                </tr>
                <tr>
                  <th></th>
                  <td></td>
                </tr>
              </table>
            </div>
            <div class="col-md-6 col-sm-6">
             <table class="customers_null">
              <tr>
                <th>Set Date/Time</th>
                <td>
                  <div class="input-group-prepend">
                    <input type="text" class="form-control"  placeholder="" id="Date_Time"  name="" maxlength="">
                    <div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                  </div>
                </td>
              </tr>
              <tr>
                <th></th>
                <td></td>
              </tr>
              <tr>
                <th></th>
                <td class="td r">
                  <button type="submit" class="btn btn-outline-success">Submit</button>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div> -->
<!-----------------------------TheEnd Row----------------------------->
<!----------------------------- SCG row 1----------------------------->
<div class = 'row'>
  <div class="col-md-12 col-sm-12">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-random"></i>&nbsp;URL</div>
        <div class="card-body">
          <div class='row'>
              <div class='col-md-6 col-sm-6'>
              <table class="customers_null">
                <tr>
				<th>MQTT Server</th>
				<td>
				<div id="name_1">
                    <!-- id = mqtt -->
                      <div class="row">
                        <!-- <i style = "margin-top: 10px;"> tcp:// </i> -->
                        <div class="col">
                          <input id = "mqtt-name" type="text" class="form-control" placeholder="Server">
                        </div>
                        <!-- <b style = "margin-top: 10px;"> : </b>
                        <div class="col">
                          <input id = "mqtt-port" type="text" class="form-control" placeholder="Port">
                        </div> -->
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th>IOT Server</th>
                  <td>
                    <div id="name_1">
						<!-- id = iot-->
						<div class="row">
							<!-- <i style = "margin-top: 10px;"> https:// </i> -->
							<div class="col">
								<input id = "iot-name" type="text" class="form-control" placeholder="Server">
							</div>
							<!-- <b style = "margin-top: 10px;"> : </b>
							<div class="col">
								<input id = "iot-port" type="text" class="form-control" placeholder="Port">
							</div> -->
						</div>
                    </div>
                  </td>
                </tr>
              </table>
              </div>
              <div class='col-md-6 col-sm-6'>
              <table class="customers_null">
                <tr>
                  <th>API Server</th>
                  <td>
                    <div id="name_1">
						<!------ id = device ----->
						<div class="row">
							<!-- <i style = "margin-top: 10px;"> https:// </i> -->
							<div class="col">
								<input id = "device-name" type="text" class="form-control" placeholder="Server">
							</div>
							<!-- <b style = "margin-top: 10px;"> : </b>
							<div class="col">
								<input id = "device-port" type="text" class="form-control" placeholder="Port">
							</div> -->
						</div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th>App Server</th>
                  <td>
                    <div id="name_1">
						<!------- id = url_g -------->
						<div class="row">
							<!-- <i style = "margin-top: 10px;"> https:// </i> -->
							<div class="col">
								<input id = "url_g-name" type="text" class="form-control" placeholder="Server">
							</div>
						</div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <th></th>
                  <td class="td r">
                  <button type="button" class="btn btn-outline-success" id="submit_click" data-toggle="modal" data-target="#modalscg">Update</button>
                  </td>
                </tr>
              </table>
              </div>
            <div class="modal fade bd-example-modal-lg" id="modalscg" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content" id="modal_content">
                  <div class="modal-header"></div>
                  <div class="modal-body"></div>
                  <div class="modal-footer"></div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!---------------------------------- SCG ----------------------------->
<!----------------------------- SCG row 2----------------------------->
<form method="post">
<div class = 'row'>
  <div class="col-md-12 col-sm-12">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-random"></i>&nbsp;Enroll Device</div>
        <div class="card-body">
          <div class='row'>
              <!-- <div class='col-md-6 col-sm-6'>
              <table class="customers_null">
                <tr>
                  <th>Username</th>
                  <td>
                    <div id="name_1">
                      <input type="text" class="form-control" id="username" value = "">
                    </div>
                  </td>
                </tr>
                <tr>
                  <th>Password</th>
                  <td>
                    <div id="name_1">
                      <input type="password" class="form-control" id="password" value = "">
                    </div>
                  </td>
                </tr>
              </table>
              </div> -->
              <div class='col-md-6 col-sm-6'>
              <table class="customers_null">
                <tr>
                  <th>TAG</th>
                  <td>
                    <div id="name_1">
                      <input type="text" class="form-control"  id="tag" value = "0">
                    </div>
                  </td>
                </tr>
                <!-- <tr>
                  <th>Client ID</th>
                  <td>
                    <div id="name_1">
                      <input type="text" disabled="disabled" class="form-control" id="cid" value = "0">
                    </div>
                  </td>
                </tr> -->
                <tr>
                  <th></th>
                  <td class="td r">
                    <button type="button" class="btn btn-outline-success" id="submit_click_end_low" data-toggle="modal" data-target="#modalend_low">Enroll Device</button>
                  </td>
                </tr>
              </table>
              </div>
            <div class="modal fade bd-example-modal-lg" id="modalend_low" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content" id="modal_content_endlow">
                  <div class="modal-header"></div>
                  <div class="modal-body"></div>
                  <div class="modal-footer"></div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
<!------------------------ SCG Device Select ------------------------>
<form method="post">
<div class = 'row'>
  <div class="col-md-12 col-sm-12">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-random"></i>&nbsp;Select Device</div>
        <div class="card-body">
          <div class='row'>
            <div class='col-md-6 col-sm-6'>
                <table class="customers_null">
                <tr>
                <th>Device Name</th>
                  <td>
                    <select class="form-control" id="scg_device_select" name="mode" onchange="">
                      <option value="" selected disabled hidden></option>
                      <!--
                      <option selected value="dhcp">dhcp</option>
                      <option value="static">static</option>
                      -->
                    </select>
                    <input type="submit" class="btn btn-outline-success" value="Refresh" name="Refresh">
                  </td>
                  </tr>
                  <tr>
                    <th>Input Max Cap</th>
                    <td>
                      <div class='row'>
                        <div class='col-md-10'>
                        <input type="text" class="form-control"  id="watt" value = "">
                        </div>
                        <div clas='col-md-2' style="margin-top: 10px;">
                          kW
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <th></th>
                  <td class="td r">
                    <button type="button" class="btn btn-outline-success" id="update_device">Update Device</button>
                  </td>
                </tr>
                </table>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
</form>
<!------------------------ SCG Device Select ------------------------>
<div class = 'row'>
  <div class="col-md-12 col-sm-12">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-random"></i>&nbsp;WiFi Setting</div>
        <div class="card-body">
          <div class='row'>
            <div class='col-md-6 col-sm-6'>
              <table class="customers_null">
                <tr>
                <th>WiFi SSID</th>
                  <td>
                    <select class="form-control" id="scg_device_select_wifi" name="wifi_name" onchange="">
                      <!--
                      <option selected value="dhcp">dhcp</option>
                      <option value="static">static</option>
                      -->
                    </select>
                  </td>
                  </tr>
                  <tr>
                    <th>Password</th>
                    <td>
                      <div class='row'>
                        <div class='col-md-12'>
                          <input type="password" class="form-control"  id="wifi_ssid">
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <th></th>
                  <td class="td r">
                    <button type="button" class="btn btn-outline-success" onclick = "scg_save_wifi()" >Update WiFi</button>
                  </td>
                </tr>
              </table>
            </div>
            <div class='col-md-6 col-sm-6'>
              <table class="customers_null">
                <tr>
                  <th>WiFi SSID Connected</th>
                  <td id = "wifi_name_show">
                  </td>
                </tr>
                <tr>
                  <th>Password</th>
                  <td id = "wifi_pass_show">
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
<!---------------------------------- SCG ----------------------------->
<!------------------------ SCG change password ------------------------>
<div class = 'row'>
  <div class="col-md-12 col-sm-12">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-random"></i>&nbsp;Change Username and Password</div>
        <div class="card-body">
          <div class='row'>
            <div class='col-md-6 col-sm-6'>
              <table class="customers_null">
                <tr>
                <th>Username</th>
                  <td>
                    <input type="text" class="form-control" id="username_up">
                  </td>
                  </tr>
                  <tr>
                    <th>Password</th>
                    <td>
                      <div class='row'>
                        <div class='col-md-12'>
                          <input type="password" class="form-control"  id="password_up">
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                  <th></th>
                  <td class="td r">
                    <button type="button" class="btn btn-outline-success" onclick = "scg_update_up(<?php echo $_SESSION['id'] ?>)">Update</button>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
  </div> 
</div>
<!------------------------ SCG Reboot Device ------------------------>
<form method="post">
<div class = 'row'>
  <div class="col-md-12 col-sm-12">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-random"></i>&nbsp;Reboot Device</div>
        <div class="card-body">
          <div class='row'>
            <div class='col-md-6 col-sm-6'>
                <table class="customers_null">
                <tr>
                <th>Reboot Device</th>
                  <td>
                    <input type="submit" class="btn btn-outline-success" value="Reboot" name="Reboot">
                  </td>
                  </tr>
                </table>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>
</form>
<!---------------------------------- SCG ----------------------------->
<script>
  function myFunction() {

   document.getElementById("Show_ip_text").value = document.getElementById("ip_address").value;
   document.getElementById("Show_sm_text").value = document.getElementById("subnet_mask").value;
   document.getElementById("Show_mode_text").value = document.getElementById("mode").value;
 }
</script>
<!--Methods-->
<div class="modal fade" id="methods_network" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <form action="database/db_Update_Network.php" method="POST">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"> Confirm and Reboot System </h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <?php
          $con = new connect();
          $con->select_Network();
          $result = $con->select_Network();
          foreach ($result as $row)
            ?>
          IP Assress <input type="text"  class="form-control" name="ip_address" id="Show_ip_text" value="<?php echo $row['ip_address'];?>" >
          Subnet Mask <input type="text"  class="form-control" name="subnet_mask" id="Show_sm_text" value="<?php echo $row['subnet_mask'];?>">
          DHCP/STATIC <input type="text"  class="form-control" name="mode" id="Show_mode_text" value="<?php echo $row['eth_mode'];?>">
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" type="submit" >Submit</button>
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </form>
</div>
</div>
</div>
</div>
<!-----------------------------TheEnd Row----------------------------->
<!------------------------------------------------------Footer------------------------------------------------------>
<?php include("Footer.php");  ?>
