<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://dev.oneweb.tech/MicroflowRest/DoAction",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "{ \"flowName\":\"SOLR_IOTE_008_V2\", \"object\":{   \"ItemType_Input\": {  \"item_type\": \"T002\"  }}}",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

$ret = json_decode($response);

// echo $ret;
$result = $ret->responseStatus;

$bb =  $ret->responseObjectsMap->SolarItem_Master_Output->ListSolarItemResult;

// echo $bb[1]->item_code;

if($result=="SUCCESS"){
    // echo "Save to file";
    $fp = fopen('./json/model.json', 'w');
    // fwrite($fp, $response);
    fwrite($fp, json_encode($bb));
    fclose($fp);
}

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}


?>