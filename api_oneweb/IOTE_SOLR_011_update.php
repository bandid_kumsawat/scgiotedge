<?php

    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://dev.oneweb.tech/MicroflowRest/DoAction",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "{\r\n    \"flowName\":\"IOTE_SOLR_011_Update\",\r\n\t\"object\":{\r\n\t     \"BOInstallmentTransactionDetail\": {\r\n\t      \"ListInstallment\": [\r\n\t         {\r\n\t            \"device_id\": \"ROOF000001\",\r\n\t            \"device_key\": \"device_key_test\",\r\n\t            \"installment_date\": \"2019-04-04\"\r\n\t         }\r\n\t      ],\r\n\t      \"ListInstallmentItem\": [\r\n\t         {\r\n\t            \"device_id\": \"ROOF000001\",\r\n\t            \"item_code\": \"RF000004\",\r\n\t            \"max_capability_value\": \"444.11\",\r\n\t            \"max_capability_unit\": \"444W\"\r\n\t         }\r\n\t      ],\r\n\t      \"ListInstallmentSubItem\": [\r\n\t         {\r\n\t            \"device_id\": \"ROOF000001\",\r\n\t            \"item_code\": \"RF000004\",\r\n\t            \"sub_item_code\": \"SUBITEM005\",\r\n\t            \"max_capability_value\": \"222.11\",\r\n\t            \"max_capability_unit\": \"222W\"\r\n\t         }\r\n\t      ]\r\n\t   }\r\n\t}\r\n}",
    CURLOPT_HTTPHEADER => array(
        "Accept: */*",
        "Cache-Control: no-cache",
        "Connection: keep-alive",
        "Content-Type: application/json",
        "Host: dev.oneweb.tech",
        "Postman-Token: 84712673-9dfb-46b6-bf8c-a7c74e4a35fb,9742e9ab-76d0-4d2f-8351-ad6dc04abee6",
        "User-Agent: PostmanRuntime/7.11.0",
        "accept-encoding: gzip, deflate",
        "cache-control: no-cache",
        "content-length: 843"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
    echo "cURL Error #:" . $err;
    } else {
    echo $response;
    }

?>