<?php

$curl = curl_init();


$today = date("Y-m-d"); 


curl_setopt_array($curl, array(
  CURLOPT_URL => "https://dev.oneweb.tech/MicroflowRest/DoAction",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "{    \"flowName\":\"SOLR_IOTE_010\",\r\n\t\"object\":{\r\n \t\"BOAvgCostPerkWh\": {      \"current_date\": \"".$today."\"  } }}",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);

$ret = json_decode($response);

$bb =  $ret->responseObjectsMap->BOAvgCostPerkWh_output;


// echo $ret;
$result = $ret->responseStatus;

if($result=="SUCCESS"){
    // echo "Save to file";
    $fp = fopen('./json/cost.json', 'w');
    fwrite($fp, json_encode($bb[0]));
    fclose($fp);
}


if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
?>

