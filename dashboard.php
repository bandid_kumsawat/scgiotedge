<?php
include("Header.php");
// include("database/connect.php");
?>
<style>
.Dashboard{
  background-color: #CE0600;
}
</style>
<!------------------------------------------------------Header------------------------------------------------------>

<div class="row">
  <div class="col-md-12 col-sm-12">
    <div class="alert" role="alert" style="background-color: #CE0600; color: #fff;" >
      <i class="fa fa-fw fa-calendar-o" aria-hidden="true"></i>
      Dashboard
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6 col-sm-6">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-calendar-minus-o"></i>&nbsp;General</div>
        <div class="card-body">
          <table class="customers" >
            <tr>
              <th class="td l">Name</th>
              <td >
                <div id="name"></div>
              </td>
            </tr>
            <tr>
              <th>Serial Number</th>
              <td>
                <div id="serial_numbe"></div>
              </td>
            </tr>
            <tr>
              <th>Product Name</th>
              <td>
                <div id="product_name"></div>
              </td>
            </tr>
            <tr>
              <th>Product Version</th>
              <td>
                <div id="product_version"></div>
              </td>
            </tr>
            <tr>
              <th>Operating System</th>
              <td>
                <div id="operating_system"></div>
              </td>
            </tr>
            <tr>
              <th>Software Version</th>
              <td>
                <div id="software_version"></div>
              </td>
            </tr>
            <tr>
              <th>Time</th>
              <td>
                <div id="day"></div>
              </td>
            </tr>
            <!-- <tr>
              <th>Current Date Time</th>
              <td>
               <div id="time_zone"></div>
             </td>
           </tr> -->
         </table>
       </div>
     </div>
   </div>


 <!-- url -->
  <!-- <div class="col-md-6 col-sm-6">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-calendar-minus-o"></i>&nbsp;URL</div>
        <div class="card-body">
          <table class="customers" >

            <tr>
              <th class="td 6">MQTT Server</th>
              <td >
                <div id="mqtt">
                </div>
              </td>
            </tr>

            <tr>
              <th>IoT Server</th>
              <td >
                <div id="iot">
                </div>
              </td>
            </tr>


            <tr>
              <th>App Server</th>
              <td >
                <div id="device">
                </div>
              </td>
            </tr>


            <tr>
              <th>API Server</th>
              <td >
                <div id="url_g">
                </div>
              </td>
            </tr>
            <div class="modal fade bd-example-modal-lg" id="modalscg" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content" id="modal_content">
                  <div class="modal-header"></div>
                  <div class="modal-body"></div>
                  <div class="modal-footer"></div>
                </div>
              </div>
            </div>
          </table>
        </div>
      </div>
    </div> -->

    <!-- end row -->
    <div class="col-md-6 col-sm-6">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-hourglass"></i>&nbsp;Processor</div>
        <div class="card-body">
         <table class="customers">
          <tr>
            <th class="td l">CPU Name</th>
            <td class="td l">
             <div id="cpu_name"></div>
           </tr>
           <tr>
            <th>Temperature</th>
            <td>
              <div id="temperature"></div>
            </td>
          </tr>
          <tr>
            <th>Current Speed
            </th>
            <td>
              <div id="current_speed"></div>
            </td>
          </tr>
          <tr>
            <th>Load Average</th>
            <td>
              <div id="load_average"></div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>



<div class="row">

  <!-- Enroll Device -->
  <!-- <div class="col-md-6 col-sm-6">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-calendar-minus-o"></i>&nbsp;Enroll Device</div>
        <div class="card-body">
          <table class="customers" >


            <tr>
              <th>TAG</th>
              <td >
                <div id="tag">
                </div>
              </td>
            </tr>

            <tr>
              <th>Client ID</th>
              <td >
                <div id="cid">
                </div>
              </td>
            </tr>
            <div class="modal fade bd-example-modal-lg" id="modalend_low" role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content" id="modal_content_endlow">
                  <div class="modal-header"></div>
                  <div class="modal-body"></div>
                  <div class="modal-footer"></div>
                </div>
              </div>
            </div>
          </table>
        </div>
      </div>
    </div> -->
</div>
<div class="row">
  <div class="col-md-6 col-sm-6">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-area-chart"></i>&nbsp;Memory Usage</div>
        <div class="card-body">
          <table class="customers">
            <tr>
              <th class="td l">Total Memory</th>
              <td class="td l">
               <div id="total_memory"></div>
             </td>
           </tr>
           <tr>
            <th>Used</th>
            <td>
             <div id="memory_used"></div>
           </td>
         </tr>
         <tr>
          <th>Free</th>
          <td>
           <div id="memory_free"></div>
         </td>
       </tr>
     </table>
   </div>
 </div>
</div>

<!-- Device SCG Show -->
<div class="col-md-6 col-sm-6">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-area-chart"></i>&nbsp;Network Connection</div>
        <div class="card-body">
          <table class="customers">

            <tr>
              <th>Status</th>
              <td>
                <div id="device_status">-</div>
              </td>
            </tr>

          </table>
        </div>
    </div>
</div>




</div>



<div class="row">
  <div class="col-md-6 col-sm-6">
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-fw fa-line-chart"></i>&nbsp;Disk Usage</div>
          <div class="card-body">
          <table class="customers">
            <tr>
              <th>Total disk</th>
              <td>
                <div id="total_disk"></div>
              </td>
            </tr>
            <tr>
              <th>Used</th>
              <td>
                <div id="used"></div>
              </td>
            </tr>
            <tr>
              <th>Free</th>
              <td>
              <div id="free"></div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-6 col-sm-6">
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-fw fa-line-chart"></i>&nbsp;Device Name List</div>
          <div class="card-body">
          <table class="table table-bordered">
      <thead>
        <tr>
          <th>Device Name</th>
          <th>kW</th>
        </tr>
      </thead>
      <tbody id = "tb_device_name">
        <!-- <tr>
          <td>John</td>
          <td>Doe</td>
        </tr> -->
      </tbody>
    </table>
      </div>
    </div>
  </div>
</div>



<div class="row">
  <div class="col-md-6 col-sm-6">
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-fw fa-line-chart"></i>&nbsp;Version Firmware</div>
          <div class="card-body">
          <table class="customers">
            <tr>
              <th>Item Version</th>
              <td>
                <div id="item_version"></div>
              </td>
            </tr>
            <!-- <tr>
              <th>Web Version</th>
              <td>
                <div id="web_version"></div>
              </td>
            </tr> -->
            <tr>
              <th>App Version</th>
              <td>
              <div id="db_version"></div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
<!--

<div class='row'>
  <div class="col-md-6 col-sm-6">
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-fw fa-rss"></i>&nbsp;Network</div>
        <div class="card-body">
          <table class="customers" >
            <tr>
              <th>Internet Status</th>
              <td>
                <div id="internet_status"></div>
              </td>
            </tr>
            <tr>
              <th>IP Address</th>
              <td>
                <div id="ip_address"></div>
              </td>
            </tr>
            <tr>
            <th>Subnet Mask</th>
            <td>
              <div id="subnet_mask"></div>
            </td>
          </tr>
          <tr>
            <th>DNS</th>
            <td>
              <div id="dns"></div>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
-->

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>
<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>
<!-- Custom scripts for this page-->
<script src="js/sb-admin-datatables.min.js"></script>

<script src="js/jquery.mask.js"></script>




<script language="JavaScript" >

  var time = 1 ;

  $( document ).ready(function() {
    setInterval(function(){ select_disk();  }, time*1000);
  });

  $( document ).ready(function() {
    setInterval(function(){ select_processor();  }, time*1000);
  });

  $( document ).ready(function() {
    setInterval(function(){ select_memory();  }, time*1000);
  });

  $( document ).ready(function() {
    setInterval(function(){ select_network();  }, time*1000);
  });

  $( document ).ready(function() {
    setInterval(function(){ select_general();  }, time*1000);
  });

  $( document ).ready(function() {
    setInterval(function(){ selcet_time();  }, time*1000);
  });
  $( document ).ready(function() {
    setInterval(function(){ client_id();  }, time*1000);
  });
  $( document ).ready(function() {
    scg_set_url();
    //setInterval(function(){ scg_set_url();  }, 60*1000);
  });
  $( document ).ready(function() {
    device_status();
    Device_name();
    setInterval(function(){ device_status();  }, 10*1000);
    setInterval(function(){ fw_version();  }, time*1000);
  });
  function fw_version(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "database/scg_fw.php",
      "method": "GET",
      "headers": {
        "cache-control": "no-cache",
        "postman-token": "150fccdd-6e49-3d73-b503-f982769ff64d"
      }
    }
    $.ajax(settings).done(function (response) {
      var v = JSON.parse(response);
      $("#item_version").text(v.ITEM);
      // $("#web_version").text(v.WEB);
      $("#db_version").text(v.EDGE_SYS);
    });
  }
  // SCG device check status interval on 10s
  function device_status(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "database/scg_device.php",
      "method": "GET"
    }

    $.ajax(settings).done(function (response) {
      console.log(response);
      try {
        console.log("------ status -------");
        var status = JSON.parse(response);
        console.log(status);
        $("#cid").text(status.clientID);
        if (status.cmd == 0){
          $("#device_status").text("");
          $("#device_status").append("");
          $("#device_status").append(
            "<b style = 'color:#5fba7d;'>[Connected]"+
            "&nbsp"+status.msg+"</b>"
          );
        }else{
          $("#device_status").text("");
          $("#device_status").append("");
          $("#device_status").append(
            "<b style = 'color:#F48024;'>[disconnect]</b>"+
            "&nbsp<i>"+status.msg+"</i>"
          );
        }
      }catch(err) {
        console.log(err);
        $("#device_status").text("ERROR");
      }

    });
  }
  function select_disk(){
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "database_json/select_disk.php",
      "method": "GET",
      "headers": {
        "cache-control": "no-cache",
        "postman-token": "150fccdd-6e49-3d73-b503-f982769ff64d"
      }
    }
    $.ajax(settings).done(function (response) {
      var select_disk = JSON.parse(response);
      // console.log(select_disk);
      // console.log(select_disk.List[0].id_disk_usage);

      // $('#id_disk_usage').text(select_disk.List[0].id_disk_usage);
      // $('#total_disk').text(select_disk.List[0].total_disk);
      // $('#used').text(select_disk.List[0].used);
      // $('#free').text(select_disk.List[0].free);

      // console.log($('#free').text());
      if($('#id_disk_usage').text() == ""){
        $('#id_disk_usage').text("Null");
      }else{
        $('#id_disk_usage').text(select_disk.List[0].id_disk_usage);
      }
      if($('#total_disk').text() == ""){
        $('#total_disk').text("Null");
      }else{
        $('#total_disk').text(select_disk.List[0].total_disk);
      }
      if($('#used').text() == ""){
        $('#used').text("Null");
      }else{
        $('#used').text(select_disk.List[0].used);
      }
      if($('#free').text() == ""){
        $('#free').text("Null");
      }else{
        $('#free').text(select_disk.List[0].free);
      }
    });
} //TheEnd function

function select_processor(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "database_json/select_processor.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "9b68d527-0de2-de0e-c772-9e7a339fa06c"
    }
  }
  $.ajax(settings).done(function (response) {
    var select_processor = JSON.parse(response);

    // $('#id_processor').text(select_processor.List[0].id_processor);
    // $('#cpu_name').text(select_processor.List[0].cpu_name);
    // $('#temperature').text(select_processor.List[0].temperature);
    // $('#current_speed').text(select_processor.List[0].current_speed);
    // $('#load_average').text(select_processor.List[0].load_average);

    if($('#id_processor').text() == ""){
      $('#id_processor').text("Null");
    }else{
      $('#id_processor').text(select_processor.List[0].id_processor);
    }
    if($('#cpu_name').text() == ""){
      $('#cpu_name').text("Null");
    }else{
      $('#cpu_name').text(select_processor.List[0].cpu_name);
    }
    if($('#temperature').text() == ""){
      $('#temperature').text("Null");
    }else{
      $('#temperature').text(select_processor.List[0].temperature);
    }
    if($('#current_speed').text() == ""){
      $('#current_speed').text("Null");
    }else{
      $('#current_speed').text(select_processor.List[0].current_speed);
    }
    if($('#load_average').text() == ""){
      $('#load_average').text("Null");
    }else{
      $('#load_average').text(select_processor.List[0].load_average);
    }
  });
} //TheEnd function

function select_memory(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "database_json/select_memory.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "99ae5e35-bd87-31ae-9718-f3132a398580"
    }
  }
  $.ajax(settings).done(function (response) {
    var select_memory = JSON.parse(response);

    // $('#id_total_memory').text(select_memory.List[0].id_total_memory);
    // $('#total_memory').text(select_memory.List[0].total_memory);
    // $('#temperature').text(select_memory.List[0].temperature);
    // $('#memory_used').text(select_memory.List[0].used);
    // $('#memory_free').text(select_memory.List[0].free);

    if($('#id_total_memory').text() == ""){
      $('#id_total_memory').text("Null");
    }else{
      $('#id_total_memory').text(select_memory.List[0].id_total_memory);
    }
    if($('#total_memory').text() == ""){
      $('#total_memory').text("Null");
    }else{
      $('#total_memory').text(select_memory.List[0].total_memory);
    }
    if($('#temperature').text() == ""){
      $('#temperature').text("Null");
    }else{
      $('#temperature').text(select_memory.List[0].temperature);
    }
    if($('#memory_used').text() == ""){
      $('#memory_used').text("Null");
    }else{
      $('#memory_used').text(select_memory.List[0].used);
    }
    if($('#memory_free').text() == ""){
      $('#memory_free').text("Null");
    }else{
      $('#memory_free').text(select_memory.List[0].free);
    }
  });
} //TheEnd function

function select_network(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "database_json/select_network.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "fd23168e-aeef-03a5-1e40-110305d16df8"
    }
  }
  $.ajax(settings).done(function (response) {
    var select_network = JSON.parse(response);

    // $('#id_network').text(select_network.List[0].id_network);
    // $('#internet_status').text(select_network.List[0].internet_status);
    // $('#ip_address').text(select_network.List[0].ip_address);
    // $('#subnet_mask').text(select_network.List[0].subnet_mask);
    // $('#dns').text(select_network.List[0].dns);

    if($('#id_network').text() == ""){
      $('#id_network').text("Null");
    }else{
      $('#id_network').text(select_network.List[0].id_network);
    }
    if($('#internet_status').text() == ""){
      $('#internet_status').text("Null");
    }else{
      $('#internet_status').text(select_network.List[0].internet_status);
    }
    if($('#ip_address').text() == ""){
      $('#ip_address').text("Null");
    }else{
      $('#ip_address').text(select_network.List[0].ip_address);
    }
    if($('#subnet_mask').text() == ""){
      $('#subnet_mask').text("Null");
    }else{
      $('#subnet_mask').text(select_network.List[0].subnet_mask);
    }
    if($('#dns').text() == ""){
      $('#dns').text("Null");
    }else{
      $('#dns').text(select_network.List[0].dns);
    }
  });
} //TheEnd function

function select_general(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "database_json/select_general.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "27f2f40c-4923-83b8-f3e3-8c5a2646e44c"
    }
  }
  $.ajax(settings).done(function (response) {
    var select_general = JSON.parse(response);

    // $('#id_general').text(select_general.List[0].id_general);
    // $('#name').text(select_general.List[0].name);
    // $('#serial_numbe').text(select_general.List[0].serial_numbe);
    // $('#product_name').text(select_general.List[0].product_name);
    // $('#product_version').text(select_general.List[0].product_version);
    // $('#operating_system').text(select_general.List[0].operating_system);
    // $('#software_version').text(select_general.List[0].software_version);

    if($('#id_general').text() == ""){
      $('#id_general').text("Null");
    }else{
      $('#id_general').text(select_general.List[0].id_general);
    }
    if($('#name').text() == ""){
      $('#name').text("Null");
    }else{
      $('#name').text(select_general.List[0].name);
    }
    if($('#serial_numbe').text() == ""){
      $('#serial_numbe').text("Null");
    }else{
      $('#serial_numbe').text(select_general.List[0].serial_numbe);
    }
    if($('#product_name').text() == ""){
      $('#product_name').text("Null");
    }else{
      $('#product_name').text(select_general.List[0].product_name);
    }
    if($('#product_version').text() == ""){
      $('#product_version').text("Null");
    }else{
      $('#product_version').text(select_general.List[0].product_version);
    }
    if($('#operating_system').text() == ""){
      $('#operating_system').text("Null");
    }else{
      $('#operating_system').text(select_general.List[0].operating_system);
    }
    if($('#software_version').text() == ""){
      $('#software_version').text("Null");
    }else{
      $('#software_version').text(select_general.List[0].software_version);
    }
  });
} //TheEnd function

function selcet_time(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "database_json/slecet_time.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "9205b06a-8e42-8087-a2ac-0b2691644312"
    }
  }
  $.ajax(settings).done(function (response) {
    var selcet_time = JSON.parse(response);

    // $('#id_time').text(selcet_time.List[0].id_time);
    // $('#day').text(selcet_time.List[0].day);
    // $('#month').text(selcet_time.List[0].month);
    // $('#year').text(selcet_time.List[0].year);
    // $('#time').text(selcet_time.List[0].time);
    // $('#time_zone').text(selcet_time.List[0].time_zone);

    if($('#id_time').text() == ""){
      $('#id_time').text("Null");
    }else{
      $('#id_time').text(selcet_time.List[0].id_time);
    }
    if($('#day').text() == ""){
      $('#day').text("Null");
    }else{
      $('#day').text(selcet_time.List[0].day);
    }
    if($('#month').text() == ""){
      $('#month').text("Null");
    }else{
      $('#month').text(selcet_time.List[0].month);
    }
    if($('#year').text() == ""){
      $('#year').text("Null");
    }else{
      $('#year').text(selcet_time.List[0].year);
    }
    if($('#time').text() == ""){
      $('#time').text("Null");
    }else{
      $('#time').text(selcet_time.List[0].time);
    }
    if($('#time_zone').text() == ""){
      $('#time_zone').text("Null");
    }else{
    /*$('#time_zone').text(selcet_time.List[0].time_zone);
      if($('#time_zone').text() =="+07"){
       $('#time_zone').text("Asia/Bangkok");
     }*/

   }
 });
} //TheEnd function

// -------------------------------------------------------------- SCG ------------------------------------------------------------------ //

var id_scg = "";
function scg_set_url(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "database/scg_url.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
      "postman-token": "9205b06a-8e42-8087-a2ac-0b2691644312"
    }
  }
  console.log(settings);
  $.ajax(settings).done(function (response) {
    var data = JSON.parse(response);
    console.log(data);


    if(data.List[1].url_broker == 0){
      $("#mqtt").text("-");
    }else{
      $('#mqtt').text(data.List[1].url_broker);
    }
    if(data.List[1].url_iot_platform == 0){
      $("#iot").text("-");
    }else{
      $("#iot").text(data.List[1].url_iot_platform);
    }
    if(data.List[1].url_mobile == 0){
      $("#device").text("-");
    }else{
      $("#device").text(data.List[1].url_mobile);
    }
    if(data.List[1].url_graphql == 0){
      $("#url_g").text("-");
    }else{
      $("#url_g").text(data.List[1].url_graphql);
    }


    // Enroll Device
    /*if(data.List[1].client_id == 0){
      $("#cid").text("-");
    }else{
      $("#cid").text(data.List[1].client_id);
    }*/
    if(data.List[1].tag == 0){
      $("#tag").text("-");
    }else{
      $("#tag").text(data.List[1].tag);
    }
    id_scg = data.List[1].id_scg;
  });
}

  function client_id(){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "database/scg_get_client_id.php",
        "method": "GET",
        "headers": {
          "cache-control": "no-cache",
          "postman-token": "150fccdd-6e49-3d73-b503-f982769ff64d"
        }
      }
      $.ajax(settings).done(function (response) {
        var data = JSON.parse(response);
        /*if (data.List[1].client_id == 0)
          $("#cid").text("-");
        else
          $("#cid").text(data.List[1].client_id);*/
      });
  }

  function Device_name(){

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "database/scg_select_device_watt.php",
      "method": "GET",
      "headers": {
        "cache-control": "no-cache",
        "postman-token": "150fccdd-6e49-3d73-b503-f982769ff64d"
      }
    }
    var settings2 = {
      "async": true,
      "crossDomain": true,
      "url": "api_oneweb/json/scg_get_json_model.php",
      "method": "GET",
      "headers": {
        "cache-control": "no-cache",
        "postman-token": "150fccdd-6e49-3d73-b503-f982769ff64d"
      }
    }
    $.ajax(settings).done(function (response) {
      var data = JSON.parse(response);
      console.log(data);
      $.ajax(settings2).done(function (response2) {
        var data2 = JSON.parse(response2);
        console.log(data2)
        $('#tb_device_name').html("");
        for (var i = 0;i < data.List.length;i++){
          for (var k = 0;k < data2.length;k++){
            // ((data.List[i].Device_name == data2[k].item_code) ? data2[k].item_display : "")
            if (data.List[i].Device_name == data2[k].item_code){
              $("#tb_device_name").append(
                '<tr><td>'+data2[k].item_display+'</td><td>'+(data.List[i].watt)+'</td></tr>'
              );
            }
          }
        }
      });
    });

  }
</script>


<!-- footer Modal-->
<footer class="sticky-footer" >
  <div class="container">
    <div class="text-center">
      <small> © SCG Cement-Building Materials Co., Ltd. </small>
    </div>
  </div>
</footer>
<!-- footer Modal-->

<!-- Logout Modal-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Logout </h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Do you want to sign out ?</div>
      <div class="modal-footer">
        <a class="btn btn-primary" href="logout.php">Logout</a>
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Logout Modal-->

</body>

</html>
