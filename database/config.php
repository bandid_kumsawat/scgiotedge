<?php

class dbConfig
{   

    public function dbConnection()
    {
     $this->conn = null;    
     try
     {
        $this->conn = new PDO('sqlite:C:\AppServ\www\scgiotedge\database\EDGE_SYS.db');
        // $this->conn = new PDO('sqlite:/var/www/html/database/EDGE_SYS.db');
 
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
        //    echo "con";
    }
    catch(PDOException $exception)
    {
        echo "Connection error: " . $exception->getMessage() ;
    }

    return $this->conn;
}
}



?>
