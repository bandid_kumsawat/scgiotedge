<?php
//include '../database_json/config.php';
include 'config_scg.php';

$arr = array();
$arr_put = array();

$i=0;

$sql = "SELECT * FROM solar_scg";
// -------------------------------------------------------------------
$result = $conn->query($sql);

while ($row = $result->fetchArray(SQLITE3_ASSOC)){
    $i++;
    if ($i > 1){
        break;
    }
	$arr[$i] = array(
        "id_scg"=>$row['id_scg'],
        "url_iot_platform"=>$row['url_iot_platform'],
        "url_mobile"=>$row['url_mobile'],
        "url_graphql"=>$row['url_graphql'],
        "url_broker"=>$row['url_broker'],
        "tag"=>$row['tag'],
        "client_id"=>$row['client_id'],
        "token"=>$row['token']
	);
}


$conn->close();
$arr_put = array("Total"=>$i,"List"=>$arr);
echo (json_encode($arr_put)) ;

?>
