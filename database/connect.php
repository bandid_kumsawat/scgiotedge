<?php
include("config.php");

class connect
{
    private $conn;
    
    public function __construct()
    {
        $database = new dbConfig();
        $db = $database->dbConnection();
        $this->conn = $db;
     } //TheEnd function 

     public function Redirect($url)
     {
        header("Location: $url");
    }

    public function select_General(){
        try
        {
            $st = $this->conn->prepare("SELECT * FROM general");
            $st->execute();
            $stm = $st->fetchAll();
            return $stm;

        }
        catch(PDOException $error)
        {
            echo $sql . "<br>" . $error->getMessage();
        }
        }//TheEnd function

        public function select_Network(){
            try
            {
                $st = $this->conn->prepare("SELECT * FROM network");
                $st->execute();
                $stm = $st->fetchAll();
                return $stm;
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

         public function select_Disk_usage(){
            try
            {
                $st = $this->conn->prepare("SELECT * FROM disk_usage");
                $st->execute();
                $stm = $st->fetchAll();
                return $stm;
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

        public function select_Processor(){
            try 
            {
                $st = $this->conn->prepare("SELECT * FROM processor");
                $st->execute();
                $stm = $st->fetchAll();
                return $stm;
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

        public function select_Memory_usage(){
            try
            {
                $st = $this->conn->prepare("SELECT * FROM memory_usage");
                $st->execute();
                $stm = $st->fetchAll();
                return $stm;
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

        public function select_Time(){
            try
            {
                $st = $this->conn->prepare("SELECT * FROM time_pi");
                $st->execute();
                $stm = $st->fetchAll();
                return $stm;
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

         public function select_User(){
            try
            {
                $st = $this->conn->prepare("SELECT * FROM user");
                $st->execute();
                $stm = $st->fetchAll();
                return $stm;
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

        public function select_User_text($user,$pass){
            try
            {
                $st = $this->conn->prepare("SELECT * FROM user WHERE user = :user and pass = :pass ");
                $st->bindparam(":user",$user);
                $st->bindparam(":pass",$pass);
                $st->execute();
                $stm = $st->fetchAll();
                return $stm;
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

          public function add_Network($id_network,$internet_status,$ip_address,$subnet_mask,$dns){
            try
            {
                $st = $this->conn->prepare("INSERT INTO network VALUES (:id_network,:internet_status,:ip_address,:subnet_mask,:dns)");
                $st->bindparam(":id_network",$id_network);
                $st->bindparam(":internet_status",$internet_status);
                $st->bindparam(":ip_address",$ip_address);
                $st->bindparam(":subnet_mask",$subnet_mask);
                $st->bindparam(":dns",$dns);

                $st->execute();
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

        public function update_Network($id_network,$ip_address,$subnet_mask,$dns,$mode){
            try
            {
                $st = $this->conn->prepare("UPDATE network SET id_network =:id_network , ip_address =:ip_address, subnet_mask = :subnet_mask , dns = :dns ,eth_mode =:mode WHERE id_network = :id_network ");
                $st->bindparam(":id_network",$id_network);
                $st->bindparam(":ip_address",$ip_address);
                $st->bindparam(":subnet_mask",$subnet_mask);
                $st->bindparam(":dns",$dns);
                $st->bindparam(":mode",$mode);

                $st->execute();
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

        public function update_user($user,$pass){
            try
            {
                $st = $this->conn->prepare("UPDATE user SET user=:user , pass =:pass WHERE user = :user ");
                $st->bindparam(":user",$user);
                $st->bindparam(":pass",$pass);
                $st->execute();
            }
            catch(PDOException $error)
            {
                echo $sql . "<br>" . $error->getMessage();
            }
        }//TheEnd function

    }




    

  // Test selectAllMenu(); 

 // $n = new Connect(); 
 // print_r($n->select_Disk_usage());





 ?>


