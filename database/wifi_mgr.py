import time
import sqlite3
import subprocess
import commands
import rpiinf
import config

def scan_wifi() :
	conn = sqlite3.connect(config.PATH_DB_MEM )
	cursor = conn.execute("SELECT * from wifi_scan")
    for row in cursor :
        print row
    print subprocess.check_output(['nmcli','dev','wifi'])


if __name__ == "__main__" :
	print "Test Wifi"
	print scan_wifi()
